Mobian uses a Gitlab-based packaging workflow. This makes sure the package
build and upload process is as automated as possible and doesn't depend on the
knowledge and/or availability of a single (or small group of) person.

# Basic principles

We maintain a generic CI pipeline in our
[packaging-tools](https://gitlab.com/mobian1/packaging-tools)
repository, which consists of 2 parts:
- the main `mobian-ci.yml` is basically the standard pipeline used on Salsa,
  Debian's GitLab instance, with a few small differences, the main ones being:
  - it uses Mobian's own [docker images](https://gitlab.com/mobian1/docker-images)
  - the `build i386` job is replaced by `build arm64` as we don't support
    `i386` and prefer to make sure packages build fine on ARM architectures
    instead
- `release-ci.yml` is included by `mobian-ci.yml` and contains the "package
  release" jobs and pipeline

All package repositories are configured to use this pipeline by default. When
pushing commits and/or branches, the "standard" pipeline runs, ensuring the new
version builds properly and passes a few (extremely) basic tests. Artifacts
from the `build arm64` job include `.deb` packages which can be downloaded and
installed to a device for further testing.

Once the test package has been validated, it can be released by pushing a tag
of the form `mobian/<upstream version>-<package version>`: when such a tag is
pushed, the CI pipeline will automatically switch to the "package release"
pipeline and build the package for all supported architectures (`amd64`,
`armhf` & `arm64`), upload those packages to our server in a temporary folder,
and finally import the packages to our archive.

That way, anyone able to directly push `mobian/*` tags to package repos (i.e.
members of the `Mobian` group with `Maintainer` rights) has the ability to
publish a package.

# Packaging workflow

## Recommended tools

It is strongly recommended that anyone working on Mobian packages use
[git-buildpackage](https://honk.sigxcpu.org/projects/git-buildpackage/manual-html/index.html)
for updating their packages. In particular, `gbp import-ref` and `gbp tag` are
especially useful in our workflow:
- `gbp import-ref` creates a `.orig` tarball from an upstream tag and stores it
  in the `pristine-tar` branch we can then directly push to the repo
- `gbp tag` automatically creates the right `mobian/*` based on the last
  `debian/changelog` entry

## Creating a new package

*Note: GitLab projects can only be created by members of the `Mobian` group with
`Maintainer` rights.*

### Importing a package from Debian

There are several cases where we could need to import a package already existing
in Debian:
- the package is stuck in Debian's NEW queue
- it is only present in `experimental` or blocked in `unstable`
- it needs patches to work as expected on mobile devices

In that case, the package should be created by executing the following steps:
- Go to the [Create new project](https://gitlab.com/projects/new) page in
  GitLab
- Select the [Import project](https://gitlab.com/projects/new#import_project)
  option
- Click the `Repo by URL` button
- Enter the URL of the package repository on [Salsa](https://salsa.debian.org/)
  in the `Git repository URL` field and select `mobian1/packages` for the
  `Project URL`
- Set visibility to `Public` and click the `Create project` button

After your project has been imported, you can clone it and create a new
`mobian` branch from `debian/master` (or `debian/latest` or `debian/sid`
depending on the convention the Debian maintainer of this package chose to
follow) you can then push to GitLab.

**Important: upstream Debian packaging branches should be left untouched and
not contain any Mobian-specific change.**

### Creating a new package for Mobian

When a software we want in Mobian isn't yet packaged for Debian, you can create
a new empty project under the [`packages`](https://gitlab.com/mobian1/packages)
subgroup and start working in this repo.

**If this package is relevant for Debian, please start working on the Debian
package first and only import it into Mobian after it has been uploaded
(but not necessarily accepted) to the Debian archive.**

Please follow the [DEP-14](https://dep-team.pages.debian.net/deps/dep14/)
recommendations, with the only notable exception is that the main branch for
Mobian must be named `mobian` and release tags must be of the form
`mobian/<upstream version>-<package version>`. Also, storing `.orig` tarballs
in the `pristine-tar` branch is mandatory for Mobian.

Kernel packages, however, follow a different branch naming scheme: the main
branch must be named `mobian-x.y` with `x.y` being the corresponding kernel
branch (e.g. `mobian-5.15` for all kernels of the `5.15` series). That way we
can keep releasing stable versions of the kernel while working on newer
branches.

### Project configuration

Once your project is created and all your default branches are pushed, make
sure the following settings are applied:
- `General` -> `Merge requests`: set `Merge method` to `Fast-forward merge`
  then click the `Save changes` button
- `Repository` -> `Default branch`: select the `mobian` branch
- `Repository` -> `Protected branches`: create a new protection for the
  `mobian` branch:
  - `Branch`: select `mobian` from the drop-down menu
  - `Allowed to merge`: select `Developers + Maintainers`
  - `Allowed to push`: select `Maintainers`
  - enable `Allowed to force push`
  - disable `Require approval from code owners`
  - click the `Protect` button
- `Repository` -> `Protected tags`: create a new protection for `mobian*` tags:
  - `Tag`: create the `mobian*` wildcard
  - `Allowed to create`: select `Maintainers` from the drop-down menu
  - click the `Protect` button
- `CI/CD` -> `General pipelines`: enter `mobian-ci.yml@mobian1/packaging-tools`
  in the `CI/CD configuration file` field then click the `Save changes` button

Finally, make sure your `debian/` folder in the `mobian` branch contains a
`gbp.conf` file with at least the following contents:
```
[DEFAULT]
debian-branch = mobian
debian-tag = mobian/%(version)s
pristine-tar = True
```

If the project has been imported from Debian and already contains such a file,
modify it with the above values and commit the changes in a dedicated commit.

## Updating an existing package

The usual procedures for updating a Debian package apply here. Due to the
nature of Mobian and how the project works as an overlay on top of Debian,
there are a few important specificities.

### Don't merge, rebase!

Except for packages for which Mobian is upstream (`mobian-tweaks`, `callaudiod`
and most packages under the `devices` subgroup), we mainly provide a few
patches on top of existing Debian packages. In order to easily keep track of
our changes, it is easier to rebase and squash our commits when a new version
of the package is released in Debian rather that keeping them buried in a
linear but deep history.

This also serves as a reminder of what should be upstreamed (or reworked in a
way which would be acceptable by upstream and/or the package's Debian
maintainer).

Kernel packages are a bit different in the sense that they're not based on
existing Debian packages (we use our own packaging rules for those). They
should therefore be rebased on top of the latest upstream tag of the current
branch.

### Check the project configuration is right

A lot of work has been done prior to implementing this workflow, and several
repos differ with the recommendations in this document. When working on
updating a package, please check all items from the
[Project configuration](#project-configuration) paragraph above have the
expected values.

### Version number suffix

As soon as a package based on an existing Debian package contains
Mobian-specific changes, you must not increment the Debian package's revision,
but instead append the `mobianX` suffix to it, where `X` is the Mobian revision
for this package. When rebasing on a new Debian release, `X` must be reset to
`1`.

When importing a package from Debian without making any Mobian-specific change,
(e.g. package stuck in NEW) this suffix should be omitted, making it clear that
we're only waiting for the package to be accepted or to transition to
`testing`.

### Create a Merge Request

When you're satisfied with your changes **and CI passes**, please open a MR for
your updated package.

Once your MR has been approved by enough people according to our
[approval rules](Developer's-Guide#packaging-for-mobian), you can then merge
your changes. As they have likely been rebased on top of the latest
Debian/upstream version, you'll have to replace the `mobian` branch in the repo
with your working branch. This can be done by executing the following commands:
```
$ git branch -D mobian
$ git branch --move <wip branch> mobian
$ git push -u origin -f mobian
```

*Note: this operation can only be executed by members of the `Mobian` group
with `Maintainer` rights. If you don't have such permissions, please ask a
maintainer to merge and release your changes for you.*

### Release the new package

A new version of a package can be released by pushing the corresponding
`mobian/<version>` tag to the repository. The recommended way of doing so is by
executing `gbp tag`. If, however, this isn't possible (packages imported from
Debian without any change), `git tag mobian/<version>` will be enough.

Once the tag has been pushed, CI will take care of building the package,
uploading it to the server and importing it into our archive.

*Note: this operation can only be executed by members of the `Mobian` group
with `Maintainer` rights.*
