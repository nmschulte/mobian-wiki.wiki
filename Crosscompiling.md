# Prerequisites
There are a few prerequisites you need in order to get the docker image running
- Your kernel must be configured with binfmt support (should be `CONFIG_BINFMT_MISC` option)
- You should install `qemu-user-static`
- You should follow this documentation [here](https://github.com/multiarch/qemu-user-static) to enable qemu-multiarch

# Running the image
Mobian provides a number of Docker images for development. You can run `docker run -u 1000:1000 -i -v <workspace-directory>:/home/debian/build -w /home/debian/build -it registry.gitlab.com/mobian1/docker-images/mobian-builder-arm64:latest bash` to start bash in the docker image. `<workspace-directory>` should be replaced by the directory (in your local filesystem) you want to have available under `/home/debian/build` (in the docker environment).

The user password for the user `mobian` is `pass`. This can be changed in the Dockerfile from https://gitlab.com/mobian1/docker-images/-/blob/master/Dockerfile.common if you are building the images yourself.