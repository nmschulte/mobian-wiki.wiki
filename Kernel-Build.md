### Prerequisites

As super user :

```console
dpkg --add-architecture arm64
apt-get update
apt-get dist-upgrade
apt-get install g++-aarch64-linux-gnu build-essential crossbuild-essential-arm64 git debootstrap u-boot-tools device-tree-compiler libncurses-dev flex bison libssl-dev rsync kmod
```

### Get sources

```console
git clone https://gitlab.com/mobian1/devices/sunxi64-linux.git pinephone-linux
cd pinephone-linux/
```

### Apply patches in debian/patches

```console
gbp pq import
```

applies those patches listed in debian/patches/series to a patch-queue/BRANCH branch ([man page](https://honk.sigxcpu.org/projects/git-buildpackage/manual-html/man.gbp.pq.html)). (quilt push is an alternative means to achive this)

### Generate .config from known working config

```console
ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu- make pine64_defconfig
```

### Customization

```console
make ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu- menuconfig
```

### Compilation

```console
ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu- make -j4 bindeb-pkg KERNELRELEASE="5.9-sunxi64-test" KDEB_PKGVERSION="1"
```

`KERNELRELEASE` should include the string `sunxi64` (in case of PineTab/PinePhone)

### Backup .config file

```console
ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu- make savedefconfig
```

### Testing debian package

You can mount with NFS your host folder or copy the debian package over ssh using scp.

On the target, you might be able to install the package with

```console
sudo dpkg -i /path_to/linux-image-5.9-sunxi64-test_1_arm64.deb
```

And then use a hard reboot to test it.

#### Troubleshooting

In the case of a boot failure, you can follow this [tutorial](https://wiki.mobian-project.org/doku.php?id=troubleshooting&s%5B%5D=chroot#general-troubleshooting) to re-enable the previous kernel by editing `/boot/extlinux/extlinux.conf` and change the `default` label to boot to the previous kernel.

## Update Process

Rebasing is used over merging so we have all our commits on top of upstream and we're less likely to forget anything when rebasing over a newer kernel branch.

The following steps are used.

* Pull the latest `mobian-5.x` branch and fetch from upstream
* Create a new branch: `git checkout -b mobian-5.x.y`
* Create the patch branch: `gbp pq import`
* Switch back to the main branch: `gbp pq switch`
* Rebase the main branch on latest upstream tag: `git rebase v5.x.y`
  * For major kernel versions: `git rebase --onto <latest upstream tag> <old upstream tag> <your working branch>` See <https://git-scm.com/book/ms/v2/Git-Branching-Rebasing> (figure 39).
* Create the new Debian-formatted upstream tag (needed for CI): `git tag upstream/5.x.y+<platform> v5.x.y^{}`
  * `<platform>` can be `sunxi64`, `librem5`, `sdm845`, `rockchip` depending on the device(s) this kernel is targeting
  * Don't forget to push the new tag: `git push origin upstream/5.x.y+<platform>`
* Rebase our patches on top of the main branch: `gbp pq rebase`
  * For major kernel versions: `git rebase --onto <your working branch> <last commit of previous mobian version> patch-queue/<your working branch>`
* Update patch files: `gbp pq export`
* Check the changes and make sure it still builds; Discard cosmetic-only changes (commit hashes, line numbers...) as long as the patches still apply cleanly without fuzz, that brings only noise for future reviews
* If some patches were dropped in the process, renumber patch files: `gbp pq export --renumber`
* Tag the upstream release with `git tag -f upstream/<version>+sunxi64 v<version>^{}` and push the tag. This allows CI to extract the source before building.