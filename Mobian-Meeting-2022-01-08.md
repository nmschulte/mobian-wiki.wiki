- (kop316) Discussion on Phone Quality in Mobian

        - Background: In using the OG PP, PPP, and L5, Calls, SMS, and Data is not created equally. There can be differences like if it handles VoLTE well, SMS drop outs, etc. It would be nice if we had some documentation on how well these are handled on the Mobian Wiki so users have some assurance on how well they handle Day to Day

        - Notes: It would be good to start a wiki of known carriers and how well they work with e.g. VoLTE, SMS, MMS, etc. so there is confidence in how well it works.

        - It would be nice to be able to sort the list of applications in the wiki by category/name or compatibility level (using a wiki plugin)

- (a-wai) Baseline work:
    - Package management
        - Notes: Overall, not too bad to do (as a-wai had the time to do it). But, it would be good to have someone else monitor it.

            - Undef: Don't focus as much on smaller point releases, but focuses on major releases.

            - a-wai: match up with Debian, and don't worry about upstream updates not lined up with Debian

            - a-wai: a bit concerned about when GNOME42 comes out....this will be a lot

            - geoclue is unreliable and not much maintained. The contents of the Mozilla Location Database might not be fresh

        - Updates monitoring

            - Notes
                    - (a-wai): Subscribed to GNOME-maintainers in debian, and to gitlab from Purism.

                        - This helps a-wai to know when things are updated and to go ahead and update.

                            - Main issue: Not automated, need to be subscribed, etc.

        - Patches tracking and upstream version updates

- (a-wai) New devices support:
    - Device selection
        - Focus only on devices that are "mainline" device and share the ethics we do
        - (kop316): Can we see if Purism has some older non-sellable devices to sell to Mobian members?
            - devrtz can look into it
        - Firmware extraction
            - (a-wai) Can we upstream non-free firmware into Debian, where can we even find the licesning for this?
                - Federico can look into it!
                - (a-wai): add to gitlab wiki which devices have which firmware to help maintaining
            - TODO: firmware extraction on Android devices (see https://gitlab.com/postmarketOS/pmaports/-/blob/master/main/msm-firmware-loader/msm-firmware-loader.sh)
            - On first boot, extract firmware to this so we don't have to care about licensing
- (a-wai) Device support maintenance
    - Kernel tracking and updates
        - (mostly) up-to-date for Pine64 devices
            - PPP: We may be able to fix FDE issue with mini-intramfs
                - u-boot 2021.10 has bug for eMMC: Has to be hard reset:
                        - Not sure where the issue is?
            - Lagging behind for L5 and SDM845 devices
                - Quick to do for minor releases 20-30 min), but more work for major releases (up to 8 hours)
                    - (a-wai) Patches are mix/intermingled and you have to look for them. This makes updating very tedious (for Purism L5)
                        - It would be easy if we tracked MRs, but that requires time.....
                        - Chews through time for a-wai
                            - (a-wai) Can we find someone on Mobian channel to help out with this?
                        - (devrtz, et al.) Are there crash courses to help?
                            - (a-wai, undef) Should only need some knowledge in C and git (mainly git)
                            - https://gitlab.com/mobian1/wiki/-/wikis/Kernel-Build
- (a-wai) non-free dependencies (see https://gitlab.com/mobian1/devices/pinephone-support/-/issues/6)
- (devrtz) PSA: Phosh contributors meeting, FODSEM devroom schedule: https://fosdem.org/2022/schedule/track/foss_on_mobile_devices/
    - Phosh contributors meeting will be on First FOSDEM Sunday -> Feb 6 2022 1600 UTC+1
- (devrtz): Can folks help out on FOSDEM to moderate (matrix/Jitsi to relay questions to the speaker), etc? Would be day of
- Workload and attracting new contributors
    - Don't know how active the community is?
        - Make Mobian dev open to all? Yes
            - Worst case, we can make another closed channel if it gets too noisy
        - Talk about the project on blogs, mastodon etc (get in touch with spaetz to share the mastodon account among a few people, so we can share some posts)
        - How that the Mobian Developers actually use it!!
        - Having pages with packages that need to be upstreamed into Debian...
        - Wiki: How to contribute page... Be detailed. Where to reach out to us, what is expected, etc.
            - Upstream first for new non-mobile specific packages
            - Contacts for mentoring (+1) (+1)
                - How to maintain a new device's kernel
- Funding? <- what can we even do with it?
    - Buy dev devices?
        - No one has time to actually 
            - Federico3 knows some folks who can do small things (including himself)
                - Also knows who is doing funding
    - Fund Upstream development
        - Add if folks are sponsoring us too
            - Pine64, Purism, etc.
            - https://nlnet.nl/project/current.html
    - Community Engagement
        - Commmunity manager
            - Moderators for Matrix/IRC
            - Route info between people
    - Do we want a Facebook and/or Twitter?
        - No Facebook, but there is a Twitter page (a-wai mainly maintains it)

Meeting notes:
- Rearrange wiki to sort on which programs are compatible (good) with apps
    - How can we do this on the fly? (aka. the browser)
        - Probably need a plugin and/or see if this is possible at all
- Geoclue package needs some more testing (f.e. GNOME maps)

    - Federico and kop316 see various shades of good and bad on it, but have no idea about why/how

    - Should we collaborate with e.g. pmOS/Manjaro to help fix these issues?