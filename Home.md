Mobian is an open-source project aimed at bringing Debian GNU/Linux to mobile
devices.

This wiki is intended to collect all useful information related to Mobian's
development. Documents related to Mobian's usage are available in
[a separate wiki](https://wiki.mobian-project.org).

# Contents

- [Contribute](Contribute)
- [Developer's Guide](Developer's-Guide)
  - [Packaging Workflow](Packaging-Workflow)
  - [Building the Kernel](Kernel-Build)
  - [Crosscompiling](Crosscompiling)
- [Roadmap](Development-Roadmap)


# License

[![Creative Commons](https://licensebuttons.net/l/by-sa/4.0/88x31.png)](https://creativecommons.org/licenses/by-sa/4.0/)

This work is licensed under a Creative Commons Attribution-ShareAlike 4.0
International License.
