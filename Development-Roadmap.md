The current focus is towards reducing the gap between Mobian and Debian. This
implies:
- avoiding disruptive changes which we know won't be accepted into Debian as
  much as possible
- upload packages in Debian where possible
- working with upstream to make sure we can, over time, drop packages from our
  archive and rely on Debian's versions instead

Areas requiring more work are:
- propose changes upstream so we can get rid of most of our tweaks, both in
  `mobian-tweaks` and `<device>-tweaks` packages
- add support for new devices, both by creating/updating device-specific
  kernel packages and by adding corresponding support to `mobian-recipes`

Other areas of interest are:
- maintain existing packages by rebasing on new releases on a regular basis
  and update/drop patches as needed (keeping an eye on patches by other
  projects is also important to ensure we ship the latest improvements and
  fixes)

# Packages to be uploaded to Debian

Apart from Mobian-specific packages, we maintain a few packages which could be
generally useful and therefore should be uploaded to Debian. Those packages are:
- `qrtr`, `pd-mapper`, `rmtfs` and `tqftpserv`: needed for Qualcomm SoC's
  (configure and communicate with DSP's)
- `calamares-extensions`
- `firefox-esr-mobile-config`
- `powersupply`
- `wake-mobile`

Mobian packages we should upload to Debian include:
- `meta-mobian`
- `mobian-tweaks`
- `mobian-plymouth-theme`
- `calamares-settings-mobian`

Please note most of those packages would need to be cleaned up and possibly
reworked in order to comply with the Debian policy (especially `meta-mobian`
and `mobian-tweaks`).

# Mobian Developers Meetings
- [Meeting on July 21, 2021](Mobian-Meeting-2021-07-27)
- [Mobian Meeting January 8, 2022](Mobian-Meeting-2022-01-08)

# Porting to new phones
Due to limited development volunteers, Mobian developers prioritize phones whose companies want to mainline their kernels and share the goals of Mobian.

However, if someone wishes to port over and maintain a new device with Mobian, we can assist with creation of the repositories. Please understand that should you choose to do this, we are asking you to keep maintaining it.
