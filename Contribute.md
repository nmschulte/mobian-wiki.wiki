# I want to contribute to Mobian
We are glad you found this page and are motivated to help out! This document will describe how you can contribute to the project.

# This is not a comprehensive list!
You may have a great idea none of us thought of before. If you have an idea and want to contribute by it, tell us your thoughts!

# I want to learn!
We are happy to help mentor you and help you. But please understand that the Mobian devs are all volunteers, so we can only "teach you to fish", and expect a fair amount of self-motivation.

# I don't have technical skills
You do not need technical skills to contribute to Mobian, and in fact, there are many very important ways you can help Mobian.

- Writing documentation
- Community engagement, such as:
    - Helping out new users
    - Help us run our Blog/Twitter/Fosstodon Channels
    - Moderating our Matrix Channels
- Run Mobian and help report bugs
- Translating upstream software

# I have basic tech skills

Please help us with:

- Triaging issues on [Gitlab](https://gitlab.com/mobian1/issues)
- Filing high quality issues upstream

# I have development experience

There's plenty of work to be done improving upstream software used in Mobian (features, bug fixes, etc). A common need is to adapt application to the size of the screen.

Additonally, help is wanted to keep the kernels of the devices we support up to date.

# I have Debian packaging experience

We always need help in:

  - Maintaining packages (we always strive for a Debian first approach)
  - Keeping device-specific kernels up-to-date
  - Adding support for new devices

We understand packaging for Debian can seem daunting. Many of the Mobian developers are also on the DebianOnMobile team and maintain those packages. You can see examples of packaging [here](https://salsa.debian.org/DebianOnMobile-team). The [Debian Policy](https://www.debian.org/doc/debian-policy/index.htm) is a great reference is you have any specific questions on packaging. As a general introduction we refer you to the [Debian wiki](https://wiki.debian.org/Packaging) on packaging which has a guide, a tutorial and more general information about packaging

If after this you are still having trouble, feel free to reach out in the Mobian Matrix Channel.

If you're looking for applications to package have a look at [our wiki](https://wiki.mobian-project.org/doku.php?id=apps) for inspiration.
Please be aware that maintaining packages is usually a long term commitment and unless you're a Debian Developer already you will need to find a sponsor for your package (preferably up front).

## Resources
*  [Debian New Maintainers Guide](https://www.debian.org/doc/manuals/maint-guide/index.html)
*  [Debian Policy Manual](https://www.debian.org/doc/debian-policy/index.html)
*  [Building with git-buildpackage](https://honk.sigxcpu.org/projects/git-buildpackage/manual-html/index.html)
*  [Packaging with git](https://wiki.debian.org/PackagingWithGit)
*  [Check for new upstream versions with debian/watch file](https://wiki.debian.org/debian/watch)
*  See how things have been done in [Salsa Debian On Mobile Team](https://salsa.debian.org/DebianOnMobile-team)
*  Use [Codesearch](https://codesearch.debian.net) to search for examples (removing files, generating manpages, etc)